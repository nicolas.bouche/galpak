Installation
============

.. include:: common.rst


Download Options
----------------

Download the latest version from http://galpak3d.univ-lyon1.fr/downloads.html.

Clone the repository using git (up coming)

Download from the Astro Common Library (up coming)

If you want to contribute, write to `Nicolas Bouché <mailto:nicolas.bouche@univ-lyon1.fr>`_.


Setup
-----
* Since v1.30, simply do ``pip install galpak``

If you download the latest from  http://galpak3d.univ-lyon1.fr/downloads.html,


* unpack and run ``pip install .``

* Add the galpak parent directory to your PYTHONPATH. (Not recommanded)

.. include:: dependencies.rst
