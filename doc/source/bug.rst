Bug reporting
*************

.. include:: common.rst

The galpak library comes with a test suite, which is the *via regala* for exposing bugs.
If your bug is related to a specific FITS file, please make sure to upload it too,
after reducing its file size as much as you can.

When in doubts, feel free to send emails to the `authors  <../authors.html>`_ of this project.
