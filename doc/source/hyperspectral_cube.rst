Hyperspectral Cube
******************

This class is a very simple (understand : not fully-featured nor exhaustive)
model of a hyperspectral cube.

.. autoclass:: galpak.HyperspectralCube
    :members: from_file, write_to, sanitize, initialize_self_cube,defaults_from_instrument, initialize_self_cube, is_empty, has_header, get_step, get_steps, wavelength_of

.. note::
    This class is not fully formed yet and its API may evolve as it moves to its own module.