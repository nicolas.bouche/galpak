Acknowledgments
---------------

If you use GalPak3D in a publication, please cite `Bouche et al. 2015 <http://adsabs.harvard.edu/abs/2015AJ....150...92B>`_
and link to the ACL entry `http://www.ascl.net/1501.014 <http://adsabs.harvard.edu/abs/2015ascl.soft01014B>`_
Please also send me a reference to your paper.