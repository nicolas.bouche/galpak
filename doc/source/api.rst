:class:`GalPaK3D <galpak.GalPaK3D>`'s API
-----------------------------------------

.. include:: common.rst

.. autoclass:: galpak.run

.. autoclass:: galpak.autorun

.. autoclass:: galpak.GalPaK3D
    :members: run_mcmc, auto_run, create_clean_cube, create_convolved_cube, plot_mcmc, plot_geweke, plot_images, plot_correlations, save

