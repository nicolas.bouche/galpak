Under the hood
==============

.. include:: common.rst

.. include:: disk_model.rst

.. include:: instruments.rst

.. include:: hyperspectral_cube.rst

.. include:: galaxy_parameters.rst

.. include:: mcmc.rst

.. include:: point_spread_functions.rst

.. include:: line_spread_functions.rst
