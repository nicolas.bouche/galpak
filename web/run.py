# -*- coding: utf-8 -*-

import io
from os import listdir
from os.path import isfile, join, abspath, dirname
from flask import Flask
from flask import redirect, url_for, send_from_directory
from flask import request
import re

from model.Config import Config
from model.News import NewsCollection, News


### PATH RELATIVITY ############################################################

runner_path = dirname(abspath(__file__))


def get_path(relative_path):
    return abspath(join(runner_path, relative_path))


### COLLECT GLOBAL INFORMATION FROM SOURCES ####################################

# VERSION
# If this fails, you're on your own. That's why we should use a VERSION file.
# When doing from galpak import, we get the OLD version number if galpak was
# also install via the setup script, as it get priority.
# Read version.py

f = open('src/galpak/__version__.py')
exec(f.read())
print("Building web for version",__version__)

# CONFIG
config_path = get_path('config')
config = Config(directory=config_path)

# sorting arrays
def sorted_aphanumeric(data):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(data, key=alphanum_key)

# DOWNLOADS
downloads_path = get_path('../build')#'../dist'
downloads_files = [f for f in listdir(downloads_path)
                   if isfile(join(downloads_path, f))]
downloads_files = sorted_aphanumeric(downloads_files)
downloads_files.reverse()
downloads_files = downloads_files[0:5]
print("Download files",downloads_files)

# NEWS
news_path = get_path('news')
news = NewsCollection(directory=news_path)


### SETUP FLASK ENGINE #########################################################

app = Flask(__name__)


### SETUP JINJA2 TEMPLATE ENGINE ###############################################

def markdown_filter(value, nl2br=False, p=True):
    """
    nl2br: set to True to replace line breaks with <br> tags
    p: set to False to remove the enclosing <p></p> tags
    """
    from vendor.markdown import markdown
    from vendor.markdown.extensions.nl2br import Nl2BrExtension
    from vendor.markdown.extensions.abbr import AbbrExtension
    extensions = [AbbrExtension()]
    if nl2br is True:
        extensions.append(Nl2BrExtension())
    markdowned = markdown(value, output_format='html5', extensions=extensions)
    if p is False:
        markdowned = markdowned.replace(r"<p>", "").replace(r"</p>", "")
    return markdowned

from jinja2 import Environment, FileSystemLoader
tpl_engine = Environment(loader=FileSystemLoader([get_path('view')]),
                         trim_blocks=True,
                         lstrip_blocks=True)

tpl_engine.filters['markdown'] = markdown_filter
tpl_engine.filters['md'] = markdown_filter

tpl_global_vars = {
    'request': request,
    'version': __version__,
    'config':  config,
    'downloads_files':  downloads_files,
}


### HELPERS ####################################################################

def render_view(view, context=None):
    """
    A simple helper to render [view] template with [context] vars.
    It automagically adds the global template vars defined above, too.
    It returns a string, usually the HTML contents to display.
    """
    if context is None:
        context = {}
    d = tpl_global_vars.copy()
    d.update(context)
    return tpl_engine.get_template(view).render(d)


def render_page(page, title="My Page", context=None):
    """
    A simple helper to render the md_page.html template with [context] vars, and
    the additional contents of `page/[page].md` in the `md_page` variable.
    It automagically adds the global template vars defined above, too.
    It returns a string, usually the HTML contents to display.
    """
    if context is None:
        context = {}
    context['title'] = title
    context['md_page'] = ''
    
    d = tpl_global_vars.copy()
    f = open(get_path('page/%s.md' % page), mode='r')
    context['md_page'] = f.read()
    d.update(context)

    return tpl_engine.get_template('md_page.html').render(d)


### ROUTING ####################################################################

@app.route("/")
@app.route("/index.html")
def index():
    return render_view('index.html', {
        'news': news.find(start=0, count=13),
    })


@app.route("/authors.html")
def authors():
    return render_view('authors.html')


@app.route("/licence.html")
def licence():
    return render_view('licence.html')


@app.route("/papers.html")
def papers():
    return render_page('papers', title='Papers')

@app.route("/alternatives.html")
def alternatives():
    return render_page('alternatives', title='Others')


@app.route("/downloads.html")
def downloads():
    changelog = ''
    fc = open(get_path('../CHANGELOG.md'))
    changelog = fc.read()
    return render_view('downloads.html', {
        'changelog': changelog,
    })


@app.route('/downloads/<path:filename>')
def downloads_filename(filename):
    return send_from_directory(downloads_path, filename)


@app.route('/doc/')
def doc_index():
    return redirect(url_for('doc', filename='index.html'))


@app.route('/doc/<path:filename>')
def doc(filename):
    return send_from_directory(get_path('../doc/build/html'), filename)


### MAIN #######################################################################

if __name__ == "__main__":
    # We can leave the debug mode on, as the production files are generated by
    # frozen-flask. We also add the YAML config and news files to the list of
    # files to watch for automatic reload.
    extra_files = [join(config_path, f) for f in listdir(config_path)
                   if isfile(join(config_path, f))]
    extra_files.extend([join(news_path, f) for f in listdir(news_path)
                        if isfile(join(news_path, f))])
    app.run(debug=True, extra_files=extra_files)
