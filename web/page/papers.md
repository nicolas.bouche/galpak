
Papers using GalPak
===================
 - All papers [citing galpak](https://ui.adsabs.harvard.edu/search/fq_database=database%3A%20astronomy&p_=0&q=citations(bibcode%3A%222015AJ....150...92B%22)&rows=25&sort=date%20desc%2C%20bibcode%20desc&start=0)
 - Papers citing galpak [from the community](https://ui.adsabs.harvard.edu/search/fq_database=database%3A%20astronomy&p_=0&q=citations(bibcode%3A%222015AJ....150...92B%22)%20-author%3ABouch%C3%A9&rows=25&sort=date%20desc%2C%20bibcode%20desc&start=0)

 Selected papers:
 
 - Lee et al. 2025 [ApJ, submitted](https://ui.adsabs.harvard.edu/abs/2024arXiv241107312L/abstract)  Disk kinematics at high redshift: DysmalPy's extension to 3D modeling and comparison with different approaches 
 - Xu, Ouchi et  al. 2024, [ApJ, 976, 142](https://ui.adsabs.harvard.edu/abs/2024ApJ...976..142X/abstract) Dynamics of a Galaxy at z > 10 Explored by JWST Integral Field Spectroscopy: Hints of Rotating Disk Suggesting Weak Feedback 
 - Herenz et al. 2024, [A&A, submitted](https://ui.adsabs.harvard.edu/abs/2024arXiv240603956H/abstract) The Lyman Alpha Reference Sample XV. Relating Ionised Gas Kinematics with Lyman-α
observables 
 - Amvrosiadis et al. 2024 [MNRAS, submitted](https://ui.adsabs.harvard.edu/abs/2023arXiv231208959A/abstract)  The kinematics of massive high-redshift dusty star-forming galaxies 
 - Ubler et al. 2024, [MNRAS, 527, 920](https://ui.adsabs.harvard.edu/abs/2024MNRAS.527.9206U/abstract)  Galaxy kinematics and mass estimates at z 1 from ionized gas and stars 
 - Puglisi et al. 2023, [MNRAS, 524, 2814](https://ui.adsabs.harvard.edu/abs/2023MNRAS.524.2814P/abstract)  KURVS: the outer rotation curve shapes and dark matter fractions of z 1.5 star-forming galaxies 
 - Weng et al. 2023, [MNRAS, 523, 676](https://ui.adsabs.harvard.edu/abs/2023MNRAS.523..676W/abstract) MUSE-ALMA Haloes XI: gas flows in the circumgalactic medium
 - Herenz  et al. 2023, [A&A, 670, 121](https://ui.adsabs.harvard.edu/abs/2023A%26A...670A.121H/abstract) A ∼15 kpc outflow cone piercing through the halo of the blue compact metal-poor galaxy SBS 0335-052E
 - Ciocan et al. 2022, [A&A, 667, 61](https://ui.adsabs.harvard.edu/abs/2022A%26A...667A..61C/abstract)   Morpho-kinematics of MACS J0416.1-2403 low-mass galaxies 
 - Bouch&eacute; et al. 2022, [A&A, accepted](https://ui.adsabs.harvard.edu/abs/2021arXiv210907545B/abstract) The MUSE Extremely Deep Field: Evidence for SFR-induced cores in dark-matter dominated galaxies at z~1
 - Bouch&eacute; et al. 2021, [A&A, 654, 49](https://ui.adsabs.harvard.edu/abs/2021A&A...654A..49B/abstract),The MUSE Hubble Ultra Deep Field Survey. XVI. The angular momentum of low-mass star-forming galaxies: A cautionary tale and insights from TNG50
 - Tejos et al. 2021, [MNRAS, 507, 663](https://ui.adsabs.harvard.edu/abs/2021MNRAS.507..663T/abstract), Telltale signs of metal recycling in the circumgalactic medium of a z~0.77 galaxy
 - Hogan et al. 2021, [MNRAS, 503, 5329](https://ui.adsabs.harvard.edu/abs/2021MNRAS.503.5329H/abstract), Integral field spectroscopy of luminous infrared main-sequence galaxies at cosmic noon 
 - Zabl et al. 2021, [MNRAS, in press](https://ui.adsabs.harvard.edu/abs/2021arXiv210514090Z/abstract), MusE GAs FLOw and Wind (MEGAFLOW) VIII. Discovery of a MgII emission halo probed by a quasar sightline 
 - Tadaki et al. 2020, [ApJ, 889, 141](https://iopscience.iop.org/article/10.3847/1538-4357/ab64f4), A Noncorotating Gas Component in an Extreme Starburst at z = 4.3
 - Fogasy et al. 2020, [MNRAS, 493, 3](https://doi.org/10.1093/mnras/staa472), SMM J04135+10277: a distant QSO-starburst system caught by ALMA
 - Zabl et al. 2020,[MNRAS, 492, 4576](https://doi.org/10.1093/mnras/stz3607), MusE GAs FLOw and Wind (MEGAFLOW) IV. A two sight-line tomography of a galactic wind
 - Wu et al. 2020, [ApJ, 887, 2](https://iopscience.iop.org/article/10.3847/1538-4357/ab5953/pdf), The Star-Forming Interstellar Medium of Lyman Break Galaxy Analogs
 - Sharon et al. 2019, [ApJ, 879, 52](https://iopscience.iop.org/article/10.3847/1538-4357/ab22b9/pdf), Resolved Molecular Gas and Star Formation Properties of the Strongly Lensed z = 2.26 Galaxy SDSS J0901+1814
 - Rybak et al. 2019, [ApJ, 876, 112](https://iopscience.iop.org/article/10.3847/1538-4357/ab0e0f), Strong Far-ultraviolet Fields Drive the [C ii]/Far-infrared Deficit in z ~ 3 Dusty, Star-forming Galaxies
 - Hatsukade et al. 2019, [ApJ, 876, 91](https://iopscience.iop.org/article/10.3847/1538-4357/ab1649/pdf), 
 - Tadaki et al. 2019, [ApJ, 876, 1](https://iopscience.iop.org/article/10.3847/1538-4357/ab1415/pdf) CNO Emission of an Unlensed Submillimeter Galaxy at z = 4.3
 - Pereira-Santaella et al. 2019, [MNRAS, 486, 5621](https://doi.org/10.1093/mnras/stz1218), Optical integral field spectroscopy of intermediate redshift infrared bright galaxies
 - Lee et al. 2019, [ApJ, 883, 92](https://iopscience.iop.org/article/10.3847/1538-4357/ab3b5b/pdf), A Radio-to-millimeter Census of Star-forming Galaxies in Protocluster 4C 23.56 at z = 2.5: Global and Local Gas Kinematics
 - Hatsukade et al. 2019, [ApJ, 876, 91](https://iopscience.iop.org/article/10.3847/1538-4357/ab1649), Molecular Gas Properties in the Host Galaxy of GRB080207 
 - Schroetter et al. 2019, [MNRAS, 490, 4368](https://ui.adsabs.harvard.edu/abs/2019MNRAS.490.4368S), MusE GAs FLOw and Wind (MEGAFLOW) III. galactic wind properties using background quasars 
 - Zabl et al. 2019, [MNRAS, 485, 196](http://adsabs.harvard.edu/abs/2019MNRAS.485.1961Z), MusE GAs FLOw and Wind (MEGAFLOW) II. A study of gas accretion around z=1 star-forming galaxies with background quasars 
 - Rivera et al. 2018, [ApJ, 863, 56](http://adsabs.harvard.edu/abs/2018ApJ...863...56C), Resolving the ISM at the Peak of Cosmic Star Formation with ALMA: The Distribution of CO and Dust Continuum in z=2.5 Submillimeter Galaxies 
 - Girard et al. 2018, [A&A, 613, 72](http://adsabs.harvard.edu/abs/2018A%26A...613A..72G), KMOS LENsing Survey (KLENS) : morpho-kinematic analysis of star-forming galaxies at z=2 
 - Klitsch et al. 2018, [MNRAS, 475, 492](http://adsabs.harvard.edu/abs/2018MNRAS.475..492K), ALMACAL III: A combined ALMA and MUSE Survey for Neutral, Molecular, and Ionised Gas in an HI-Absorption-Selected System 
 - Rahmani et al. 2018, [MNRAS, 474, 254](http://adsabs.harvard.edu/abs/2018MNRAS.474..254R), Observational signatures of a warped disk associated with cold-flow accretion
 - Mason et al. 2017, [ApJ, 838, 14](http://adsabs.harvard.edu/abs/2017ApJ...838...14M), First Results from the KMOS Lens-Amplified Spectroscopic Survey (KLASS): Kinematics of Lensed Galaxies at Cosmic Noon
 - Finley et al. 2017, [A&A, 605, 118](http://adsabs.harvard.edu/abs/2017A%26A...605A.118F), Galactic winds with MUSE: A direct detection of Fe II* emission from a z = 1.29 galaxy 
 - Schroetter et al. 2016, [ApJ, 833, 39](http://adsabs.harvard.edu/abs/2016ApJ...833...39S), Muse Gas Flow and Wind (MEGAFLOW). I. First MUSE Results on Background Quasars
 - Contini et al. 2016, [A&A, 591, 49](http://adsabs.harvard.edu/abs/2016A%26A...591A..49C), Kinematics of low-mass galaxies up to z=1.4 from deep MUSE observations in the HDFS
 - Bouch&eacute; et al. 2016, [ApJ, 820, 121](http://adsabs.harvard.edu/abs/2016ApJ...820..121B), Possible Signatures of a Cold-flow Disk from MUSE Using a z=1 Galaxy-Quasar Pair toward SDSS J1422-0001
 - Martin & Soto 2016, [ApJ, 819, 49](http://adsabs.harvard.edu/abs/2016ApJ...819...49M), Resolving Gas Flows In The ULIRGSIRAS23365+3604 With KECK LGSAO/OSIRIS, (Resolving Gas Flows in the Ultraluminous Starburst IRAS 23365+3604 with Keck LGSAO/OSIRIS
 - Bolatto et al. 2015, [ApJ, 809, 175](http://adsabs.harvard.edu/abs/2015ApJ...809..175B), _High Resolution Imaging of PHIBSS z=2 Main Sequence Galaxies in CO(1-0)
 - Schroetter et al. 2015, [ApJ, 804, 83](http://adsabs.harvard.edu/abs/2015ApJ...804...83S),  _The VLT SINFONI MgII Program for Line Emitters (SIMPLE) II: Background Quasars probing Z=1 winds_
 - Bacon et al. 2015, [A&A, 575, A75](http://adsabs.harvard.edu/abs/2015A%26A...575A..75B), _The MUSE 3D view of the Hubble Deep Field South_
 - P&eacute;roux et al. 2013, [MNRAS, 436, 2650](http://adsabs.harvard.edu/abs/2013MNRAS.436.2650P), _A SINFONI integral field spectroscopy survey for galaxy counterparts to damped Lyman alpha systems - IV. Masses and gas flows_ 
 - Bouch&eacute; et al. 2013, [Science 341, 6141](http://www.sciencemag.org/content/341/6141/50.abstract?ijkey=3xk5V5lwMFjBY&keytype=ref&siteid=sci), _Signatures of Cool Gas Fueling a Star-Forming Galaxy at Redshift 2.3_
 
Please send a note if a publication is using GalPak to [this email](mailto:nicolas.bouche@univ-lyon1.fr).
