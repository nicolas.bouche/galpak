
Alternative 3D algorithms
=========================

Other 3D algorithms have been developped recently such as
 
 - [DYSMALPy](https://www.mpe.mpg.de/resources/IR/DYSMALPY/index.html): DysmalPy (DYnamical Simulation and Modelling ALgorithm in PYthon) is a Python-based forward modeling code designed for analyzing galaxy kinematics originally inspired by Reinhard Genzel’s DISDYN program (e.g., Tacconi et al. 1994), maintained at MPE, and extends the IDL-based DYSMAL fitting models introduced in previous works (Davies et al. 2011).
 - [CONDOR](https://github.com/juancho9303/CONDOR): CONDOR is a kinematic fitting code that finds the best kinematic model of a rotating disk galaxy by combining its high- and low-resolution velocity fields, which is optimized for data acquired with facilities that use adaptive optics (AO) suchs as OSIRIS (Keck) and SINFONI (VLT).
 - [TiRiFic](https://gigjozsa.github.io/tirific/) : Old gipsy code from the Groningen team from [Jozsa et al. 2007](https://doi.org/10.1051/0004-6361:20066164);
 - [3DBarollo](http://editeodoro.github.io/Bbarolo) : A 3D tilded ring model with PSF kernel from [Di Deodoro & Fraternali 2015](https://doi.org/10.1093/mnras/stv1213);
 - [GBKFIT](https://supercomputing.swin.edu.au/portfolio/gbkfit/) : 3D model with GPU based fitting without a PSF kernel from [Bekiaris, Glazebrook et al. 2016](https://academic.oup.com/mnras/article/455/1/754/984505)
 - DYSMAL : [Davies et al. 2011](http://adsabs.harvard.edu/abs/2011ApJ...741...69D)
 - Blobby3D(UNVAILABLE) : non-parameteric flux (multi-gaussian) with parametric V(r) from [Varidel et al. 2019](https://doi.org/10.1093/mnras/stz670) using [DNEST4](https://github.com/eggplantbren/DNest4) sampler from  [Brewer et al. 2018](http://dx.doi.org/10.18637/jss.v086.i07);
 - UNAVAILABLE: A 3D model with parametric V(r) and with lensing [Rizzo et al. 2018](https://doi.org/10.1093/mnras/sty2594);
 - UNAVAILABLE: A hybrid 2D approach on maps with lensing [Patricio et al. 2018](https://doi.org/10.1093/mnras/sty555);
 - UNAVAILABLE: A non-parametric algorithm for 1D rotation curves [Hernandez et al. 2019](https://doi.org/10.1093/mnras/stz1969)
