__version__ = '1.35.2'
# Please follow the semver guidelines : http://semver.org
# Many parts of the galpak package use this version number :
# - galpak.__version__ itself, obviously
# - the website generator
# - the documentation generator (sphinx)
# If you need to use this version number with another language than python,
# you'll have to write a parser. galpak needs to integrate into MPDAF
# whose setup prevents the usage of the preferred VERSION file.
