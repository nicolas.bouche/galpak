# coding=utf-8


class AnsiColors:
    """
    A simple set of ANSI colors as escape sequences, stolen from Blender.
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'